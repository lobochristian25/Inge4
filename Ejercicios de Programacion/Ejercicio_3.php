<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Ejercicios de Programacion" >
    <title > Ejercicio 1</title >
</head >
<body >
    <?php 
    /*
    3- Ejercicio 3:
    • Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1
    y N.
    • El número N estará definido por una constante PHP. El valor de la constante N debe ser un
    número definido por el alumno.
    El script PHP debe estar embebido en una página HTML.
*/
        $n = 100;

        echo '<div style="display: flex; flex-direction: row; justify-content: center; align-items: center; width: 100%; height: 90vh"><span style="width: 200px;">';
        $s = '<table border = 1>';
        $c = 0;

        for ($i=1; $i <= $n; $i++) {
            if ($i%2 == 0) {
                $c++;
                if ($c == 0)
                    $s .= '<tr>';
                $s .= "<td> {$i} </td>";
                if ($c == 5) {
                    $s .= '</tr>';
                    $c = 0;
                }
            }
            
        }
        $s .= '</table>';
        echo $s;

        echo '</span></div>'
    ?>
</body >
</html >