<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Ejercicios de Programacion" >
    <title > Ejercicio 1</title >
</head >
<body >
    <?php 
    /*
    1- Ejercicio 1:
    Hacer un script PHP que realice el siguiente cálculo
    x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x
    Donde:
    • A es la raiz cuadrada de 2
    • ¶ es el número PI
    • B es es la raíz cúbica de 3
    • C es la constante de Euler
    • D es la constante e
    Observación: Utilizar las constantes matemáticas definidas den la extensión math de PHP
*/
        $pi = M_PI;
        $a = sqrt(2);
        $b = pow(3, 1/3);
        $c = M_EULER;
        $d = M_E;

        $x = (($a * $pi) + $b) / ($c*$d);
        echo '<div> $x = ((', $a, '*', $pi, ') + ', $b, ') / (', $c, '*', $d, ') </div>';
        echo '<div>', $x, '</div>';
    ?>
</body >
</html >