<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Ejercicios de Programacion" >
    <title > Ejercicio 2</title >
</head >
<body >
    <?php 
    /*
    2- Ejercicio 2:
    Hacer un script PHP que imprime la siguiente información:
    • Versión de PHP utilizada.
    • El id de la versión de PHP.
    • El valor máximo soportado para enteros para esa versión.
    • Tamaño máximo del nombre de un archivo.
    • Versión del Sistema Operativo.
    • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
    • El include path por defecto.
    Observación: Ver las constantes predefinidas del núcleo de PHP
*/

        if (!defined('PHP_VERSION_ID')) {
            $version = explode('.', PHP_VERSION);

            define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
        }

        $version = phpversion();
        $id = PHP_VERSION_ID;
        $val_max = PHP_INT_MAX; // PHP_INT_SIZE;
        $length = strlen(basename( __FILE__ ));
        $version_so = php_uname();
        $simbol = PHP_EOL;
        $path = getcwd();

        echo '<div> Version: ', $version, '</div>';
        echo '<div> Version_ID: ', $id, '</div>';
        echo '<div> Valor máximo: ', $val_max, '</div>';
        echo '<div> Tamaño max del nombre de archivo: ', $length, '</div>';
        echo '<div> Version del SO: ', $version_so, '</div>';
        echo '<div> Simbolo: ', $simbol, '</div>';
        echo '<div> Path: ', $path, '</div>';
    ?>
</body >
</html >