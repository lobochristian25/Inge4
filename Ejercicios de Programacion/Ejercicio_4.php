<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Ejercicios de Programacion" >
    <title > Ejercicio 4</title >
</head >
<body >
    <?php 
    /*
    4-Ejercicio 4:
    Hacer un script PHP que haga lo siguiente:
    • Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
    valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
    • Ordenar de mayor a menor los valores de estas variables.
    • Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
    separados por espacios en blanco.
    • El número mayor debe estar en color verde y el número más pequeño debe estar en color
    rojo.
*/

        function NroAleatorio()
        {
            return mt_rand(50, 900);
        }
        $a = NroAleatorio();
        $b = NroAleatorio();
        $c = NroAleatorio();

        
        $array = array($a, $b, $c);

        sort($array, 1);

        foreach ($array as $key => $value) {
            echo '<div>', $value, '</div>';
        }
    ?>
</body >
</html >