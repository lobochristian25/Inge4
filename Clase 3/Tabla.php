<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Mi Primer Script PHP" >
    <title > Mi Primer Script PHP </title >
    <style>

    
    table {
            border: 1px solid blue;
            /*table-layout: fixed;*/
            width: 350px;
        }
    td {text-align: center; }
    tr:nth-child(even){background: #A8A8A8;}
    tr:nth-child(odd){background:D4E6F1;}
    
    .titulo {
        background-color: yellow !important;
        text-align: center;
    }
    .subtitulo {
        background-color: #8E8530 !important;
        text-align: center;
    }
    </style>
</head >
<body >
    <?php

$data = [
        
    [
        'nombre' => 'Coca Cola',
        'cantidad' => '100',
        'precio' => '4.500'
    ],  
    [
        'nombre' => 'Pepsi',
        'cantidad' => '30',
        'precio' => '4.800'
    ],  
    [
        'nombre' => 'Sprite',
        'cantidad' => '20',
        'precio' => '4.500'
    ],  
    [
        'nombre' => 'Guaraná',
        'cantidad' => '200',
        'precio' => '4.500'
    ],  
    [
        'nombre' => 'SevenUp',
        'cantidad' => '24',
        'precio' => '4.800'
    ],  
    [
        'nombre' => 'Mirinda Naranja',
        'cantidad' => '56',
        'precio' => '4.800'
    ],  
    [
        'nombre' => 'Mirinda Guaraná',
        'cantidad' => '89',
        'precio' => '4.800'
    ],  
    [
        'nombre' => 'Fanta Naranja',
        'cantidad' => '10',
        'precio' => '4.500'
    ],  
    [
        'nombre' => 'Guaraná',
        'cantidad' => '2',
        'precio' => '4.500'
    ],
];


    echo '<div>';
    echo '<h3>Tabla del 9 </h3>';
    $s = '<table>';
    $s .= '<tr class="titulo" ><th colspan="3">Productos</th></tr>';
    $s .= '<tr class="subtitulo">';
    $s .= '<th>Nombre</th>';
    $s .= '<th>Cantidad</th>';
    $s .= '<th>Precio (Gs)</th></tr>';
    foreach ( $data as $r ) {
        $s .= '<tr >';
        foreach ( $r as $v ) {
                $s .= '<td>'.$v.'</td>';
        }
        $s .= '</tr>';
    }
    $s .= '</table>';
    echo '<br>', $s;
    ?>
</body >
</html >
