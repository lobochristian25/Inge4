<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 4</title >
</head >
<body >
    <?php 
    /*
    4-Ejercicio 4:
    Hacer un script en PHP que haga lo siguiente:
    • El script PHP debe estar embebido en una página HTML
    • Crear una variable con el siguiente contenido 24.5
    • Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
    (poner un carácter de nueva línea)
    • En la siguiente línea del script, modificar el contenido de la variable al valor “HOLA”
    Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
    (poner un carácter de nueva línea)
    • Setear el tipo de la variable que contiene “HOLA” a un tipo entero
    • Determinar con var_dump el contenido y tipo de esa variable}
*/
        $variable = 24.5;
        
        echo '<u>Variable:</u> ', $variable, "<br>";
       	echo '<u>Tipo de dato:</u> ',gettype($variable), "<br><br>";

        $variable = 'HOLA';
           
       	echo '<u>Nuevo valor de variable:</u> ',$variable, "<br>";
       	echo '<u>Tipo de dato:</u> ',gettype($variable), "<br><br>";

       	$variable = 123;
       	var_dump($variable);
    ?>
</body >
</html >