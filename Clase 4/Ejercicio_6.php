
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ejercicio 6">
    <title>Ejercicio 6</title>
</head>
<body>
    <?php /*
6- Ejercicio 6:
Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
• Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
estar entre 99 y 999). Las variables serán $a, $b y $c
• Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
expresión $b+$c
• Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
que la expresión $a*3
 */

    $a = rand(99,999);
    $b = rand(99,999);
    $c = rand(99,999);

    // echo ($a*3) > ($b+$c) ? $a*3 : $b+$c ;
    echo '<div>Variable A: '.$a.'<br> Variable B: '.$b.'<br> Variable C: '.$c.'<br><br></div>';
        echo 'A x 3: '.($a*3).'<br>';
        echo 'B + C: '.($b+$c).'<br><br><br>';
        echo ($a*3) > ($b+$c) ?
        '$a*3 es mayor que la expresión $b+$c ' :
        '$b+$c es mayor o igual que la expresión $a*3 ';
    ?>
</body>
</html>