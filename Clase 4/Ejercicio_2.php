<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 2 </title >
    <style>
        p:{color: red}; 
    </style>
</head >
<body >
    <?php 
    /*
    2- Ejercicio 2:
    Hacer un script PHP que haga lo siguiente:
    • El script PHP debe estar embebido en una página HTML
    • Crear una variable que almacene su nombre y apellido.
    • Crear una variable que guarde su nacionalidad.
    • Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su
    nombre y apellido. El contenido se debe desplegar en negrita y en color rojo.
    • Usar la función print para imprimir en pantalla el contenido de la variable que almacena su
    nacionalidad. El contenido se debe desplegar subrayado.
 */
        $nombre = 'Christian Lobo';
        $nacionalidad = 'Paraguaya';
        
        echo '<p ><b>'.$nombre.'</b></p><br>';

        print ('<u>'.$nacionalidad.'</u>');
    ?>
</body >
</html >