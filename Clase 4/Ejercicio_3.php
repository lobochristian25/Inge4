<!DOCTYPE html >
<html lang = "es" >
<head >
    <meta charset = "UTF-8" >
    <meta name = "description" content = "Clase 4" >
    <title > Ejercicio 3</title >
</head >
<body >
    <?php 
    /*
    3- Ejercicio 3:
    • El script PHP debe estar embebido en una página HTML
    • Hacer un script PHP que concatene dos cadenas con el operador punto (.) e imprimir su
    resultado. Cada cadena debe estar contenida en una variable diferente.
 */
        $cadena1 = 'Buenas noches. ';
        $cadena2 = 'Como estas?';
        
        echo $cadena1.$cadena2;
       
    ?>
</body >
</html >