<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ejercicio 6">
    <title>Ejercicio 6</title>

    <style>
        table {
            border: 1px solid blue;
            table-layout: fixed;
            width: 180px;  
        }
        td {text-align: center; }
        tr:nth-child(even){background:#808B96;}
	    tr:nth-child(odd){background:D4E6F1;}

    </style>
</head>
<body>
    <?php
    /*
    8- Ejercicio 8:
Hacer un script en PHP que imprima 900 números aleatorios pares
Se deben generar números aleatorios entre 1 y 10.000
    */
    $a = rand(0,30);
    $b = rand(0,20);
    $c = rand(0,50);

    $r = $a+$b+$c;

    echo 'Puntaje 1: '.$a.'<br>';
    echo 'Puntaje 2: '.$b.'<br>';
    echo 'Puntaje 3: '.$c.'<br><br>';

    echo 'Total: '.$r.'<br><br>';

    switch($r) {
        case ($r >= 0 && $r <= 59):
            echo 'Nota 1';
            break;
        case ($r >= 60 && $r <= 69):
            echo 'Nota 2';
            break;
        case ($r >= 70 && $r <= 79):
            echo 'Nota 3';
            break;
        case ($r >= 80 && $r <= 89):
            echo 'Nota 4';
            break;
        case ($r >= 90 && $r <= 100):
            echo 'Nota 5';
            break;
    }
    ?>
</body>
</html>