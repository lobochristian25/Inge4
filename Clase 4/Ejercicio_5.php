
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Tabla">
    <title>Tabla</title>

    <style>
        table {
            border: 1px solid blue;
            table-layout: fixed;
            width: 180px;  
        }
        td {text-align: center; }
        tr:nth-child(even){background:#808B96;}
	    tr:nth-child(odd){background:D4E6F1;}

    </style>
</head>
<body>
    <?php /*/
5-Ejercicio 5:
• El script PHP debe estar embebido en una página HTML
• Hacer un script en PHP que muestre en pantalla la tabla de multiplicar el 9, coloreando las
filas alternando gris y blanco
 */

    $data = [
        
        [
            'a' => '9',
            'x' => 'x',
            'b' => '1',
            '=' => '=',
            'r' => '9',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '2',
            '=' => '=',
            'r' => '18',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '3',
            '=' => '=',
            'r' => '27',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '4',
            '=' => '=',
            'r' => '36',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '5',
            '=' => '=',
            'r' => '45',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '6',
            '=' => '=',
            'r' => '54',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '7',
            '=' => '=',
            'r' => '63',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '8',
            '=' => '=',
            'r' => '72',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '9',
            '=' => '=',
            'r' => '81',
        ],
        [
            'a' => '9',
            'x' => 'x',
            'b' => '10',
            '=' => '=',
            'r' => '90',
        ]
    ];

    echo '<div>';
    echo '<h3>Tabla del 9 </h3>';
    $s = '<table>';
    foreach ( $data as $r ) {
        $s .= '<tr >';
        foreach ( $r as $v ) {
                $s .= '<td>'.$v.'</td>';
        }
        $s .= '</tr>';
    }
    $s .= '</table>';
    echo '<br>', $s;
    ?>
</body>
</html>