

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ejercicio 6">
    <title>Ejercicio 6</title>

    <style>
        table {
            border: 1px solid blue;
            table-layout: fixed;
            width: 180px;  
        }
        td {text-align: center; }
        tr:nth-child(even){background:#808B96;}
	    tr:nth-child(odd){background:D4E6F1;}

    </style>
</head>
<body>
    <?php
    /*$Id$
    8- Ejercicio 8:
    Hacer un script en PHP que imprima 900 números aleatorios pares
    Se deben generar números aleatorios entre 1 y 10.000
    */

    $c = 0;
    while($c < 900) {
        $x = rand(1, 10000);
        if ($x % 2 == 0) {
            $c = $c+1;
            
            echo '<div>No.'.$c.': '.$x.'</div>';
        }

    }
    ?>
</body>
</html>